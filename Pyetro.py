__author__ = "WiDo"

# Pyetro Transducer - a Python Transcribing Tool
# March, 2015  -  by WillahScott @ WiDo

import pygame
from pygame.locals import *
from sys import exit

pygame.init()
screen = pygame.display.set_mode((640, 480), 0, 24)
pygame.display.set_caption("Pyetro Transducer v.0")
create = pygame.font.SysFont("rockwell", 30)
title = create.render("Pyetro Transducer v.0", True, (0, 0, 255), (0, 0, 0))
create2 = pygame.font.SysFont("rockwell", 20)
subtitle = create2.render("by WiDo", True, (0, 0, 180), (0, 0, 0))
icon = pygame.image.load("img/trns.png").convert()

newIcon = pygame.transform.scale(icon, (50, 50))

screen.blit(newIcon, (20, 14))
screen.blit(title, (150, 10))
screen.blit(subtitle, (240, 45))
pygame.display.update()

while True:
    for i in pygame.event.get():
        if i.type == QUIT:
            exit()

